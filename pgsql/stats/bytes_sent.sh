#!/usr/bin/env bash

# Uncomment for debug this script:
#set -x

##############################################
## Application: pgBouncer Statistics
## Item key: pgbouncer.stats.bytes_sent
## Item name: Average sent (to clients) bytes
##############################################

psql -qAtX -h "$1" -p "$2" -U "$3" -d "pgbouncer" -c "SHOW STATS_AVERAGES;" | awk -v pool=$4 -F '|' '$1==pool {print $4}'
