#!/usr/bin/env bash

# Uncomment for debug this script:
#set -x

############################################
## Application: pgBouncer Statistics
## Item key: pgbouncer.stats.xact_time
## Item name: Average transaction duration
############################################

psql -qAtX -h "$1" -p "$2" -U "$3" -d "pgbouncer" -c "SHOW STATS_AVERAGES;" | awk -v pool=$4 -F '|' '$1==pool {print $5}'
