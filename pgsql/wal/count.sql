---------------------------------------
--
-- Application: PG WAL Informations
-- Item key: pgsql.wal.count
-- Item name: WAL file count
--
---------------------------------------

SELECT count(*)
FROM pg_ls_waldir();
