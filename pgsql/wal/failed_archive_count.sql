-----------------------------------------------------------
--
-- Application: PG WAL Informations
-- Item key: pgsql.wal.failed_archive_count
-- Item name: WAL files are failed to archive
--
-----------------------------------------------------------

SELECT failed_count
FROM pg_stat_archiver
WHERE last_failed_time > last_archived_time;
