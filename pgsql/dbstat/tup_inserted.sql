----------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.tup_inserted
-- Item name: [{#DBNAME}] Rows inserted by queries
--
----------------------------------------------------------------------------------

SELECT tup_inserted
FROM pg_catalog.pg_stat_database
WHERE datname = :DATABNAME;
