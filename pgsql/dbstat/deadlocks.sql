---------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.deadlocks
-- Item name: [{#DBNAME}] Deadlocks detected
--
---------------------------------------------------------------------------


SELECT deadlocks
FROM pg_catalog.pg_stat_database
WHERE datname = :DATABNAME;
