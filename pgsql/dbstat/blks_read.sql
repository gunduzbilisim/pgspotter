-------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: PG Database Statistics
-- Item key: pgsql.dbstat.blks_read
-- Item name: [{#DBNAME}] Disk blocks read
--
-------------------------------------------------------------------------

SELECT blks_read
FROM pg_catalog.pg_stat_database
WHERE datname = :DATABNAME;
