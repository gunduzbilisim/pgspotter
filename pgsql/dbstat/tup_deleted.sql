--------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.tup_deleted
-- Item name: [{#DBNAME}] Rows deleted by queries
--
--------------------------------------------------------------------------------

SELECT tup_deleted
FROM pg_catalog.pg_stat_database
WHERE datname = :DATABNAME;
