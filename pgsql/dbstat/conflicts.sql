---------------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.conflicts
-- Item name: [{#DBNAME}] Queries canceled due to conflicts with recovery
--
---------------------------------------------------------------------------------------------------------

SELECT conflicts
FROM pg_catalog.pg_stat_database
WHERE datname = :DATABNAME;
