----------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.size
-- Item name: [{#DBNAME}] Database Size
--
----------------------------------------------------

SELECT pg_database_size(:DATABNAME);
