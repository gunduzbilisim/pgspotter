---------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.tup_returned
-- Item name: [{#DBNAME}] Rows returned by queries
--
---------------------------------------------------------------------------------

SELECT tup_returned
FROM pg_catalog.pg_stat_database
WHERE datname = :DATABNAME;
