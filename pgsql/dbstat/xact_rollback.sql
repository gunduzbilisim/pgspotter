-------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.xact_rollback
-- Item name: [{#DBNAME}] Transactions that have been rolled back
--
-------------------------------------------------------------------------------------------------

SELECT xact_rollback
FROM pg_catalog.pg_stat_database
WHERE datname = :DATABNAME;
