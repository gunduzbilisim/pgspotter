---------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.tup_updated
-- Item name: [{#DBNAME}] Rows updated by queries
--
---------------------------------------------------------------------------------

SELECT tup_updated
FROM pg_catalog.pg_stat_database
WHERE datname = :DATABNAME;
