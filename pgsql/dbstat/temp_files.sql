--------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.temp_files
-- Item name: [{#DBNAME}] Temporary files created by queries
--
--------------------------------------------------------------------------------------------

SELECT temp_files
FROM pg_catalog.pg_stat_database
WHERE datname = :DATABNAME;
