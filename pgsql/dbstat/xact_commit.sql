-------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.xact_commit
-- Item name: [{#DBNAME}] Transactions that have been committed
--
-------------------------------------------------------------------------------------------------

SELECT xact_commit
FROM pg_catalog.pg_stat_database
WHERE datname = :DATABNAME;
