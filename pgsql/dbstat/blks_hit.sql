------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.blks_hit
-- Item name: [{#DBNAME}] Disk blocks were found already in the buffer cache
--
------------------------------------------------------------------------------------------------

SELECT blks_hit
FROM pg_catalog.pg_stat_database
WHERE datname = :DATABNAME;
