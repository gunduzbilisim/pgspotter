--------------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.temp_bytes
-- Item name: [{#DBNAME}] Total amount of data written to temporary files by queries
--
--------------------------------------------------------------------------------------------------------

SELECT temp_bytes
FROM pg_catalog.pg_stat_database
WHERE datname = :DATABNAME;
