--------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.tup_fetched
-- Item name: [{#DBNAME}] Rows fetched by queries
--
--------------------------------------------------------------------------------

SELECT tup_fetched
FROM pg_stat_database
WHERE datname = :DATABNAME;
