---------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.checksum_failures
-- Item name: [{#DBNAME}] Checksum failures detected
--
---------------------------------------------------------------------------


SELECT checksum_failures
FROM pg_catalog.pg_stat_database
WHERE datname = :DATABNAME;
