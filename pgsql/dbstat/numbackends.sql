------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat_numbackends
-- Item name: [{#DBNAME}] Backends currently connected
--
------------------------------------------------------------------------------------------------

SELECT numbackends
FROM pg_catalog.pg_stat_database
WHERE datname = :DATABNAME;
