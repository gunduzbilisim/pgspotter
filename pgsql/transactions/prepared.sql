-------------------------------------------
--
-- Application: PG Transactions
-- Item key: pgsql.transactions.prepared
-- Item name: Prepared transactions
--
-------------------------------------------

SELECT count(*) 
FROM pg_prepared_xacts;
