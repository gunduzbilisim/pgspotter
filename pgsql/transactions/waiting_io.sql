--------------------------------------------
--
-- Application: PG Transactions
-- Item key: pgsql.transactions.waiting_io
-- Item name: Transaction is waiting IO 
--
--------------------------------------------

SELECT count(*)
FROM pg_catalog.pg_stat_activity
WHERE wait_event_type = 'IO'
AND state != 'idle';
