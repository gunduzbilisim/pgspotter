--------------------------------------------
--
-- Application: PG Transactions
-- Item key: pgsql.transactions.waiting_extension
-- Item name: Transaction is waiting Extension
--
--------------------------------------------

SELECT count(*)
FROM pg_catalog.pg_stat_activity
WHERE wait_event_type = 'Extension'
AND state != 'idle';
