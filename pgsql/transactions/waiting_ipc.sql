--------------------------------------------
--
-- Application: PG Transactions
-- Item key: pgsql.transactions.waiting_ipc
-- Item name: Transaction is waiting IPC 
--
--------------------------------------------

SELECT count(*)
FROM pg_catalog.pg_stat_activity
WHERE wait_event_type = 'IPC'
AND state != 'idle';
