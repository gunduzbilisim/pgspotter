--------------------------------------------
--
-- Application: PG Transactions
-- Item key: pgsql.transactions.waiting_client
-- Item name: Transaction is waiting Client
--
--------------------------------------------

SELECT count(*)
FROM pg_catalog.pg_stat_activity
WHERE wait_event_type = 'Client'
AND state != 'idle';
