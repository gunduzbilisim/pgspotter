--------------------------------------------
--
-- Application: PG Transactions
-- Item key: pgsql.transactions.waiting_bufferpin
-- Item name: Transaction is waiting BufferPin
--
--------------------------------------------

SELECT count(*)
FROM pg_catalog.pg_stat_activity
WHERE wait_event_type = 'BufferPin'
AND state != 'idle';
