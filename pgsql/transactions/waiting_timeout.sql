--------------------------------------------
--
-- Application: PG Transactions
-- Item key: pgsql.transactions.waiting_timeout
-- Item name: Transaction is waiting Timeout
--
--------------------------------------------

SELECT count(*)
FROM pg_catalog.pg_stat_activity
WHERE wait_event_type = 'Timeout'
AND state != 'idle';
