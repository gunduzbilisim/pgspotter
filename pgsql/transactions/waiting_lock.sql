--------------------------------------------
--
-- Application: PG Transactions
-- Item key: pgsql.transactions.waiting_lock
-- Item name: Transaction is waiting Lock
--
--------------------------------------------

SELECT count(*)
FROM pg_catalog.pg_stat_activity
WHERE wait_event_type = 'Lock'
AND state != 'idle';
