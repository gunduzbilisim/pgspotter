------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.n_dead_tup
-- Item name: [{#SCHNAME} / {#TBLNAME}] Estimated number of dead rows
--
------------------------------------------------------------------------

SELECT coalesce(n_dead_tup,0)
FROM pg_catalog.pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
