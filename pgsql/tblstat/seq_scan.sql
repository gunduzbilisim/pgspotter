---------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.seq_scan
-- Item name: [{#SCHNAME} / {#TBLNAME}] Sequential scans
--
---------------------------------------------------------------------

SELECT coalesce(seq_scan,0)
FROM pg_catalog.pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
