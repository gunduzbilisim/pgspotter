--------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.vacuum_count
-- Item name: [{#SCHNAME} / {#TBLNAME}] Times this table has been manually vacuumed
--
--------------------------------------------------------------------------------------------------

SELECT coalesce(vacuum_count,0)
FROM pg_catalog.pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
