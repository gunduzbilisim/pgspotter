-----------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.heap_blks_read
-- Item name: [{#SCHNAME} / {#TBLNAME}] Disk blocks read
--
-----------------------------------------------------------------------------------------

SELECT coalesce(heap_blks_read,0)
FROM pg_catalog.pg_statio_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
