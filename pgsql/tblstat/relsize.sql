-----------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: PG Table Statistics
-- Item key: pgsql.tblstat.relsize
-- Item name: [{#SCHNAME} / {#TBLNAME}] Total table size with indexes
--
-----------------------------------------------------

SELECT pg_total_relation_size(:SCHTBLNAME);
