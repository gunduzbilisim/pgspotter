------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.idx_blks_hit
-- Item name: [{#SCHNAME} / {#TBLNAME}] Buffer hits in all indexes
--
------------------------------------------------------------------------------------------------

SELECT coalesce(idx_blks_hit,0)
FROM pg_catalog.pg_statio_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
