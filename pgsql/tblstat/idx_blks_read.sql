-----------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.idx_blks_read
-- Item name: [{#SCHNAME} / {#TBLNAME}] Disk blocks read from all indexes
--
-----------------------------------------------------------------------------------------------------

SELECT coalesce(idx_blks_read,0)
FROM pg_catalog.pg_statio_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
