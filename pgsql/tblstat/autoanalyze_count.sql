------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.analyze_count
-- Item name: [{#SCHNAME} / {#TBLNAME}] Table has been analyzed by the autovacuum daemon
--
------------------------------------------------------------------------------------------------

SELECT coalesce(autoanalyze_count,0)
FROM pg_catalog.pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
