-------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.idx_tup_fetch
-- Item name: [{#SCHNAME} / {#TBLNAME}] Live rows fetched by index scans
--
-------------------------------------------------------------------------------------

SELECT coalesce(idx_tup_fetch,0)
FROM pg_catalog.pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
