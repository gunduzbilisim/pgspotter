-------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.seq_tup_read
-- Item name: [{#SCHNAME} / {#TBLNAME}] Live rows fetched by sequential scans
--
-------------------------------------------------------------------------------------------

SELECT coalesce(seq_tup_read,0)
FROM pg_catalog.pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
