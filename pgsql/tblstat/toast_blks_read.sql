------------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.toast_blks_read
-- Item name: [{#SCHNAME} / {#TBLNAME}] Disk blocks read from this table's TOAST table
--
------------------------------------------------------------------------------------------------------

SELECT coalesce(toast_blks_read,0)
FROM pg_catalog.pg_statio_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
