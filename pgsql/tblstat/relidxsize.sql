-----------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.relidxsize
-- Item name: [{#SCHNAME} / {#TBLNAME}] Total table size without indexes
--
-----------------------------------------------------

SELECT pg_table_size(:SCHTBLNAME);
