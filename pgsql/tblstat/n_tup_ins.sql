--------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.n_tup_ins
-- Item name: [{#SCHNAME} / {#TBLNAME}] Rows inserted
--
--------------------------------------------------------------------

SELECT coalesce(n_tup_ins,0)
FROM pg_catalog.pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
