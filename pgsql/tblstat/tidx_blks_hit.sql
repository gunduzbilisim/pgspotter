------------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.tidx_blks_hit
-- Item name: [{#SCHNAME} / {#TBLNAME}] Buffer hits in this table's TOAST table indexes
--
------------------------------------------------------------------------------------------------------

SELECT coalesce(tidx_blks_hit,0)
FROM pg_catalog.pg_statio_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
