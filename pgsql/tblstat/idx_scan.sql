------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.idx_scan
-- Item name: [{#SCHNAME} / {#TBLNAME}] Index scans initiated
--
------------------------------------------------------------------------------------------

SELECT coalesce(idx_scan,0)
FROM pg_catalog.pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
