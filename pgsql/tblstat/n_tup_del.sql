------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.n_tup_del
-- Item name: [{#SCHNAME} / {#TBLNAME}] Rows deleted
--
------------------------------------------------------------------

SELECT coalesce(n_tup_del,0)
FROM pg_catalog.pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
