---------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.heap_blks_hit
-- Item name: [{#SCHNAME} / {#TBLNAME}] Buffer hits
--
---------------------------------------------------------------------------------

SELECT coalesce(heap_blks_hit,0)
FROM pg_catalog.pg_statio_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
