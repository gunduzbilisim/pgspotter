--------------------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.tidx_blks_read
-- Item name: [{#SCHNAME} / {#TBLNAME}] Number of disk blocks read from this table's TOAST table indexes
--
--------------------------------------------------------------------------------------------------------------

SELECT coalesce(tidx_blks_read,0)
FROM pg_catalog.pg_statio_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
