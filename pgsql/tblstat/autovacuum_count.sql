--------------------------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: Table Statistics
-- Item key: pgsql.tblstat.autovacuum_count
-- Item name: [{#SCHNAME} / {#TBLNAME}] Table has been vacuumed by the autovacuum daemon
--
--------------------------------------------------------------------------------------------------------------------

SELECT coalesce(autovacuum_count,0)
FROM pg_catalog.pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
