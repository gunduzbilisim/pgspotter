----------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Lock Informations
-- Item key: pgsql.lock.share_row_exclusive
-- Item name: [{#DBNAME}] Share Row Exclusive locks
--
----------------------------------------------------

SELECT
    count(*)
FROM
    pg_catalog.pg_locks
WHERE
    mode = 'ShareRowExclusiveLock';
