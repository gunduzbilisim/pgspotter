----------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Lock Informations
-- Item key: pgsql.lock.share_lock
-- Item name: [{#DBNAME}] Share locks
--
----------------------------------------------------

SELECT count(*)
FROM pg_catalog.pg_locks
WHERE mode = 'ShareLock';
