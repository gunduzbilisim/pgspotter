----------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Lock Informations
-- Item key: pgsql.lock.access_exclusive
-- Item name: [{#DBNAME}] Access Exclusive locks
--
----------------------------------------------------

SELECT count(*)
FROM pg_catalog.pg_locks
WHERE mode = 'AccessExclusiveLock';
