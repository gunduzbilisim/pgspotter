#!/usr/bin/env bash

# Uncomment for debug this script:
#set -x

########################################################################################
## Application: pgBouncer Pools
## Item key: pgbouncer.pools.sv_used
## Item name: Server connections that have been idle for more than server_check_delay 
#########################################################################################

psql -qAtX -h "$1" -p "$2" -U "$3" -d "pgbouncer" -c "SHOW POOLS;" | awk -v pool="$4" -F '|' '$1==pool {sum += $7} END {print sum}'
