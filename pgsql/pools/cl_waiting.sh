#!/usr/bin/env bash

# Uncomment for debug this script:
#set -x

#######################################
## Application: pgBouncer Pools
## Item key: pgbouncer.pools.cl_waiting
## Item name: Waiting clients
#######################################

psql -qAtX -h "$1" -p "$2" -U "$3" -d "pgbouncer" -c "SHOW POOLS;" | awk -v pool="$4" -F '|' '$1==pool {sum += $4} END {print sum}'

