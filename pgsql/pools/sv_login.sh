#!/usr/bin/env bash

# Uncomment for debug this script:
#set -x

###########################################################################
## Application: pgBouncer Pools
## Item key: pgbouncer.pools.sv_login
## Item name: Server connections currently in the process of logging in
############################################################################

psql -qAtX -h "$1" -p "$2" -U "$3" -d "pgbouncer" -c "SHOW POOLS;" | awk -v pool="$4" -F '|' '$1==pool {sum += $9} END {print sum}'
