#!/usr/bin/env bash

# Uncomment for debug this script:
#set -x

#######################################
## Application: pgBouncer Pools
## Item key: pgbouncer.pools.sv_active
## Item name: Active server connections
#######################################

psql -qAtX -h "$1" -p "$2" -U "$3" -d "pgbouncer" -c "SHOW POOLS;" | awk -v pool="$4" -F '|' '$1==pool {sum += $5} END {print sum}'
