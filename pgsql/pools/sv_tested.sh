#!/usr/bin/env bash

# Uncomment for debug this script:
#set -x

############################################################################################################
## Application: pgBouncer Pools
## Item key: pgbouncer.pools.sv_tested
## Item name: Server connections that are currently running either server_reset_query or server_check_query
#############################################################################################################

psql -qAtX -h "$1" -p "$2" -U "$3" -d "pgbouncer" -c "SHOW POOLS;" | awk -v pool="$4" -F '|' '$1==pool {sum += $8} END {print sum}'
