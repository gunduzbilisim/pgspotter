----------------------------------------------------
--
-- Discovery rule: Citus Table Discovery
-- Application: Citus Table Statistics
-- Item key: citus.tblstat.relidxsize
-- Item name: [{#SCHNAME} / {#TBLNAME}] Distributed table size without indexes
--
-----------------------------------------------------

SELECT citus_table_size(:SCHTBLNAME);

