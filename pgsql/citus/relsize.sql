-----------------------------------------------------
--
-- Discovery rule: Citus Table Discovery
-- Application: Citus Table Statistics
-- Item key: citus.tblstat.relsize
-- Item name: [{#SCHNAME} / {#TBLNAME}] Total distributed table size with indexes
--
-----------------------------------------------------

SELECT citus_total_relation_size(:SCHTBLNAME);
