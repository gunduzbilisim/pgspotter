--------------------------------------------------------------------------------------
--
-- Discovery rule: PG Streaming Replication
-- Application: PG Streaming Replication
-- Item key: pgsql.streaming.state
-- Item name: [{#STANDBY}] Current WAL sender state
--
--------------------------------------------------------------------------------------

SELECT
    state
FROM
    pg_stat_replication
WHERE
    client_addr = :REPNAME;
