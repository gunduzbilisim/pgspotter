-----------------------------------------------------------------------------
--
-- Discovery rule: PG Streaming Replication
-- Application: PG Streaming Replication
-- Item key: pgsql.streaming.write_lag
-- Item name: [{#STANDBY}] Flushed recent WAL and server has written it
--
-----------------------------------------------------------------------------

SELECT
    write_lag
FROM
    pg_stat_replication
WHERE
    client_addr = :REPNAME;
