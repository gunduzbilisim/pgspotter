--------------------------------------------
--
-- Application: PG Streaming Replication
-- Item key: pgsql.streaming.recovery
-- Item name: Recovery state
--
--------------------------------------------

SELECT pg_is_in_recovery();
