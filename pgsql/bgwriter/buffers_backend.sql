---------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.buffers_backend
-- Item name: Buffers written directly by a backend
--
---------------------------------------------------------------

SELECT buffers_backend
FROM pg_catalog.pg_stat_bgwriter;
