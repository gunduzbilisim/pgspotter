-----------------------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.buffers_maxwritten_clean
-- Item name: Background writer stopped a cleaning scan
--
-----------------------------------------------------------------------------

SELECT maxwritten_clean
FROM pg_catalog.pg_stat_bgwriter;
