---------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.buffers_clean
-- Item name: Buffers written by the background writer
--
---------------------------------------------------------------

SELECT buffers_clean
FROM pg_catalog.pg_stat_bgwriter;
