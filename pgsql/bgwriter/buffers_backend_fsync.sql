---------------------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.buffers_backend_fsync
-- Item name: Backend had to execute its own fsync call
--
---------------------------------------------------------------------------

SELECT buffers_backend_fsync
FROM pg_catalog.pg_stat_bgwriter;
