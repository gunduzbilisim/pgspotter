------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.buffers_alloc
-- Item name: Buffers allocated
--
------------------------------------------------

SELECT buffers_alloc
FROM pg_catalog.pg_stat_bgwriter;
