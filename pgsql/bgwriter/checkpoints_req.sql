-------------------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.checkpoints_req[
-- Item name: Requested checkpoints that have been performed
--
-------------------------------------------------------------------------

SELECT checkpoints_req
FROM pg_catalog.pg_stat_bgwriter;
