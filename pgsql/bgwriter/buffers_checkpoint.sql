---------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.buffers_checkpoint
-- Item name: Buffers written during checkpoints
--
---------------------------------------------------------------

SELECT buffers_checkpoint
FROM pg_catalog.pg_stat_bgwriter;
