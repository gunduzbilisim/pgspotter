------------------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.checkpoints_timed
-- Item name: Scheduled checkpoints that have been performed
--
------------------------------------------------------------------------

SELECT checkpoints_timed
FROM pg_catalog.pg_stat_bgwriter;
