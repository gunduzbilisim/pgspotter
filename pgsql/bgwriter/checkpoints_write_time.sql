-------------------------------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.checkpoints_write_time
-- Item name: Time that has been spent checkpoint where files are written to disk
--
-------------------------------------------------------------------------------------

SELECT checkpoint_write_time
FROM pg_catalog.pg_stat_bgwriter;
