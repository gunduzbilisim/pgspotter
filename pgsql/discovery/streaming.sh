#/usr/bin/bash

# Uncomment for debug this script:
#set -x

########################################################
# Discovery name: PG Streaming Replication Discovery
# Discovery key: pgsql.discovery.streaming
########################################################

STANDBYLIST=$(psql -qAtX -h "$1" -p "$2" -U "$3" -d "$4" -c "SELECT client_addr FROM pg_stat_replication;")

echo -n '{"data":[';
for standby in $STANDBYLIST; do
  echo -n "{\"{#STANDBY}\": \"$standby\"},";
done | sed -e 's:,$::';
echo -n ']}'
