#!/usr/bin/env bash

# Uncomment for debug this script:
#set -x

############################################
#
# Discovery name: pgBouncer Pool Discovery
# Discovery key: pgbouncer.discovery.pool
#
############################################

POOLLIST=$(psql -qtX -h "$1" -p "$2" -U "$3" -d "pgbouncer" -c "SHOW POOLS;" | awk '{print $1}' | uniq)

echo -n '{"data":[';

for pool in $POOLLIST; do
   echo -n "{\"{#POOL}\": \"$pool\"},";
done | sed -e 's:,$::';
echo -n ']}'
