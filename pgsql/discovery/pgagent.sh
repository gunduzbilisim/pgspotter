#/usr/bin/env bash

# Uncomment for debug this script:
#set -x

########################################################
# Discovery name: pgAgent Job Discovery
# Discovery key: pgsql.discovery.pgagent
########################################################

JOBIDLIST=$(psql -qAtX -h "$1" -p "$2" -U "$3" -d "$4" -c "SELECT jobid FROM pgagent.pga_job;")

echo -n '{"data":[';
  for jobid in $JOBIDLIST; do
    JOBNAMELIST=$(psql -qAtX -h "$1" -p "$2" -U "$3" -d "$4" -c "SELECT jobname FROM pgagent.pga_job WHERE jobid = $jobid;")
    for jobname in $JOBNAMELIST; do
        echo -n "{\"{#JOBID}\": \"$jobid\",\"{#JOBNAME}\": \"$jobname\"},";
    done
  done | sed -e 's:,$::';
echo -n ']}'
