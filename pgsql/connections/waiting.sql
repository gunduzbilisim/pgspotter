--------------------------------------------
--
-- Application: PG Connections
-- Item key: pgsql.connections.waiting
-- Item name: Waiting connections
--
--------------------------------------------

SELECT count(*)
FROM pg_catalog.pg_stat_activity
WHERE state = 'waiting';
