---------------------------------------
--
-- Application: PG Connections
-- Item key: pgsql.connections.idle
-- Item name: Idle connections
--
---------------------------------------

SELECT count(*)
FROM pg_catalog.pg_stat_activity
WHERE state = 'idle';
