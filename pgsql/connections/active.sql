---------------------------------------
--
-- Application: PG Connections
-- Item key: pgsql.connections.active
-- Item name: Active connections
--
---------------------------------------

SELECT count(*)
FROM pg_catalog.pg_stat_activity
WHERE state = 'active';
