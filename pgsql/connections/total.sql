-----------------------------------------
--
-- Application: PG Connections
-- Item key: pgsql.connections.total
-- Item name: Total connections
--
-----------------------------------------

SELECT count(*)
FROM pg_catalog.pg_stat_activity;
