------------------------------------------------------
--
-- Application: PG Connections
-- Item key: pgsql.connections.idle_in_transaction
-- Item name: Idle in transaction connections
--
------------------------------------------------------

SELECT count(*)
FROM pg_catalog.pg_stat_activity
WHERE state = 'idle in transaction';
