------------------------------------------------
--
-- Discovery rule: pgAgent Job Discovery
-- Application: pgAgent Job Statistics
-- Item key: pgsql.pgagent.jobstatus
-- Item name: [{#JOBID} / {#JOBNAME}] Job status
--
------------------------------------------------

SELECT jlgstatus
FROM pgagent.pga_joblog
WHERE jlgjobid = :JOBID
ORDER BY jlgstart DESC
LIMIT 1;
