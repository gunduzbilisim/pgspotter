## Item prototypes based on discovery rules

DDiscovery rule | Application name | Item name | Description | Create enabled
--- | --- | --- | --- | ---
PG Database Discovery | {#DBNAME} - Extension Statistics | [{#DBNAME}] Total time the statement spent writing blocks (pg_stat_statements) | Total time the statement spent writing blocks, in milliseconds (if track_io_timing is enabled, otherwise zero) | No
PG Database Discovery | {#DBNAME} - Extension Statistics | [{#DBNAME}] Total number of blocks written by the statement (pg_stat_statements) | Total time the statement spent reading blocks, in milliseconds (if track_io_timing is enabled, otherwise zero) | No
PG Database Discovery | {#DBNAME} - Extension Statistics | [{#DBNAME}] Total time the statement spent reading blocks (pg_stat_statements) | Total number of blocks read by the statement (pg_stat_statements) | No
PG Database Discovery | {#DBNAME} - Extension Statistics | [{#DBNAME}] [{#DBNAME}] Total number of blocks read by the statement (pg_stat_statements) | [{#DBNAME}] [{#DBNAME}] Total number of blocks read by the statement (pg_stat_statements) | No
PG Database Discovery | {#DBNAME} - Extension Statistics | [{#DBNAME}] Other time spent in the statement (mostly cpu) (pg_stat_statements) | Other time spent in the statement (mostly cpu) (pg_stat_statements) | No
PG Database Discovery | {#DBNAME} - Extension Statistics | [{#DBNAME}] Total number of blocks dirtied by the statement (pg_stat_statements) | [{#DBNAME}] Total number of blocks dirtied by the statement (pg_stat_statements) | No
PG Database Discovery | {#DBNAME} - Extension Statistics | [{#DBNAME}] Average time spent in the statement (pg_stat_statements) | Average time spent in the statement, in milliseconds (pg_stat_statements) | No
PG Database Discovery | {#DBNAME} - Lock Informations | [{#DBNAME}] Share Update Exclusive locks | Acquired by vacuum, concurrent indexes, statistics, and some variants of the alter table commands. This mode protects a table against concurrent schema changes and vacuum runs. | Yes
PG Database Discovery | {#DBNAME} - Lock Informations | [{#DBNAME}] Share Row Exclusive locks | This mode protects a table against concurrent data changes, and is self-exclusive so that only one session can hold it at a time. Acquired by create collation, create trigger, and many forms of alter table. | Yes
PG Database Discovery | {#DBNAME} - Lock Informations | [{#DBNAME}] Share locks | Acquired by create index that is not executed in concurrent mode. This mode protects a table against concurrent data changes. | Yes
PG Database Discovery | {#DBNAME} - Lock Informations | [{#DBNAME}] Row Share locks | Acquired by the SELECT FOR UPDATE and SELECT FOR SHARE queries. | Yes
PG Database Discovery | {#DBNAME} - Lock Informations | [{#DBNAME}] Row Exclusive locks | Acquired by queries that modify the data in a table. Typically, update, delete, and insert queries | Yes
PG Database Discovery | {#DBNAME} - Lock Informations | [{#DBNAME}] Exclusive locks | This mode allows only concurrent ACCESS SHARE locks, i.e., only reads from the table can proceed in parallel with a transaction holding this lock mode. | Yes
PG Database Discovery | {#DBNAME} - Lock Informations | [{#DBNAME}] Access Share locks | Acquired by queries that only read from a table but do not modify it. Typically, this is a select query. | Yes
PG Database Discovery | {#DBNAME} - Lock Informations | [{#DBNAME}] Access Exclusive locks | This mode guarantees that the holder is the only transaction accessing the table in any way. Acquired by DROP TABLE, ALTER TABLE, VACUUM FULl commands. | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | [{#DBNAME}] Number of transactions in this database that have been rolled back | Number of transactions in this database that have been rolled back | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | [{#DBNAME}] Number of transactions in this database that have been committed | Number of transactions in this database that have been committed | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | [{#DBNAME}] Number of rows updated by queries in this database | Number of rows updated by queries in this database | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | [{#DBNAME}] Number of rows returned by queries in this database | Number of rows returned by queries in this database | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | [{#DBNAME}] Number of rows inserted by queries in this database | Number of rows inserted by queries in this database | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | 	[{#DBNAME}] Number of rows fetched by queries in this database | Number of rows fetched by queries in this database | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | [{#DBNAME}] Number of rows deleted by queries in this database | Number of rows deleted by queries in this database | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | [{#DBNAME}] Number of temporary files created by queries in this database | Number of temporary files created by queries in this database. All temporary files are counted, regardless of why the temporary file was created (e.g., sorting or hashing), and regardless of the log_temp_files setting. | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | [{#DBNAME}] Total amount of data written to temporary files by queries in this database | Total amount of data written to temporary files by queries in this database. All temporary files are counted, regardless of why the temporary file was created, and regardless of the log_temp_files setting. | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | [{#DBNAME}] Database Size | Database Size | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | [{#DBNAME}] Number of backends currently connected to this database | Number of backends currently connected to this database, or NULL for the shared objects. This is the only column in this view that returns a value reflecting current state; all other columns return the accumulated values since the last reset. | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | [{#DBNAME}] Number of deadlocks detected in this database | Number of deadlocks detected in this database | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | [{#DBNAME}] Number of queries canceled due to conflicts with recovery in this database | Number of queries canceled due to conflicts with recovery in this database. (Conflicts occur only on standby servers; see pg_stat_database_conflicts for details.) | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | [{#DBNAME}] Number of disk blocks read in this database | Number of disk blocks read in this database | Yes
PG Database Discovery | {#DBNAME} - Database Statistics | [{#DBNAME}] Number of times disk blocks were found already in the buffer cache | Number of times disk blocks were found already in the buffer cache, so that a read was not necessary (this only includes hits in the PostgreSQL buffer cache, not the operating system's file system cache) | Yes
PG Database Discovery | {#DBNAME} - Extension Statistics | [{#DBNAME}] Count of used pages (pg_buffercache) | Count of used pages (pg_buffercache) | No
PG Database Discovery | {#DBNAME} - Extension Statistics | [{#DBNAME}] Count of all pages (pg_buffercache) | Count of all pages (pg_buffercache) | No
PG Database Discovery | {#DBNAME} - Extension Statistics | [{#DBNAME}] Count of dirty pages (pg_buffercache) | Count of dirty pages (pg_buffercache) | No
PG Database Discovery | {#DBNAME} - Extension Statistics | [{#DBNAME}] Count of clear pages (pg_buffercache) | Count of clear pages (pg_buffercache) | No
PG Schema Discovery | {#DBNAME} - Schema Statistics | [{#DBNAME} / {#SCHNAME}] Schema Size | Schema Size | No
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of times this table has been manually vacuumed | Number of times this table has been manually vacuumed (not counting VACUUM FULL) | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of disk blocks read from this table's TOAST table | Number of disk blocks read from this table's TOAST table (if any) | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of buffer hits in this table's TOAST table | Number of buffer hits in this table's TOAST table (if any) | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of disk blocks read from this table's TOAST table indexes | Number of disk blocks read from this table's TOAST table indexes (if any) | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | 	[{#SCHNAME} / {#TBLNAME}] Number of buffer hits in this table's TOAST table indexes | Number of buffer hits in this table's TOAST table indexes (if any) | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Table Size | Table Size | No
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of live rows fetched by sequential scans | Number of live rows fetched by sequential scans | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of sequential scans | Number of sequential scans initiated on this table | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of rows updated | Number of rows updated (includes HOT updated rows) | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of rows inserted | Number of rows inserted | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of rows HOT updated | Number of rows HOT updated (i.e., with no separate index update required) | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of rows deleted | Number of rows deleted | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Estimated number of live rows | Estimated number of live rows | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Estimated number of dead rows | Estimated number of dead rows | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of live rows fetched by index scans | Number of live rows fetched by index scans | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of index scans initiated on this table | Number of index scans initiated on this table | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of disk blocks read from all indexes on this table | Number of disk blocks read from all indexes on this table | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of buffer hits in all indexes on this table | Number of buffer hits in all indexes on this table | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of disk blocks read from this table | Number of disk blocks read from this table | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of buffer hits in this table | Number of buffer hits in this table | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of times this table has been vacuumed by the autovacuum daemon | Number of times this table has been vacuumed by the autovacuum daemon | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of times this table has been analyzed by the autovacuum daemon | Number of times this table has been analyzed by the autovacuum daemon | Yes
PG Table Discovery | {#DBNAME} - Table Statistics | [{#SCHNAME} / {#TBLNAME}] Number of times this table has been manually analyzed | Number of times this table has been manually analyzed | Yes
