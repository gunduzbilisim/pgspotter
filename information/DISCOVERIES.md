## Discovery Rule List

Discovery name | Status
--- | ---
PG Database Discovery | Enabled
PG Schema Discovery | Disabled
PG Streaming Replication Discovery | Disabled
PG Table Discovery | Disabled
