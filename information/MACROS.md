## Macros

Macro | Value | Description
--- | --- | ---
{$PGCACHEHIT_THRESHOLD} | 90 | Give an alert when cache hit ratio's average value lower than this value
{$PGCHECKPOINTS_THRESHOLD} | 60 | Give an alert when checkpoint request last value bigger than this value
{$PGCONNECTIONS_THRESHOLD} | 200 | Give an alert when total connection last value is bigger than this value
{$PGDBSIZE_THRESHOLD} | 1073741824 | Give an alert when database size last value is bigger than this value
{$PGDEADLOCK_THRESHOLD} | 10 | Give an alert when deadlock last value is bigger than this value
{$PGODESTXID_TRESHOLD} | 18000000 | Give an alert when oldest transaction id last value is bigger than this value
{$PGOLDESTQUERY_TRESHOLD} | 18000 | Give an alert when oldest query last value is bigger than this value
{$PGSCHEMASIZE_THRESHOLD} | 1073741824 | Give an alert when schema size last value is bigger than this value
{$PGTEMPBYTES_THRESHOLD} | 8388608 | Give an alert when temp by size last value is bigger than this value
{$PG_DATABASE} | postgres | PostgreSQL atabase name for connection
{$PG_HOST} | /var/run/postgresql | PostgreSQL server host for connection (default: socket connection)
{$PG_PORT} | 5432 | PostgreSQL port for connection
{$PG_USER} | pgspotter | PostgreSQL user for connection
{$SCRIPT_PATH} | /etc/zabbix/pgspotter | pgSpotter script path for executing SQL and bash scripts
