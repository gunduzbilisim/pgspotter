## Trigger List

Severity | Trigger name | Status
--- | --- | ---
Information | Cache hit ratio is too low on {HOST.NAME} | Enabled
Information | Checkpoints are occurring too frequently on {HOST.NAME} | Enabled
Information | Many connections are forked on {HOST.NAME} | Enabled
High | No ping from PostgreSQL for 3 minutes {HOSTNAME} | Enabled
Warning | PostgreSQL count files in ./archive_status more than 2 on {HOSTNAME} | Disabled
High | PostgreSQL oldest xid is too big on {HOSTNAME} | Enabled
High | PostgreSQL process is not running on {HOST.NAME} | Enabled
Warning | PostgreSQL query running is too old on {HOSTNAME} | Enabled
Warning | PostgreSQL service was restarted on {HOSTNAME} (uptime={ITEM.LASTVALUE}) | Enabled
