## Item List

Application name | Item name | Description | Status
--- | --- | --- | ---
PG Background Writer Statistics | Time that has been spent checkpoint where files are written to disk | Total amount of time that has been spent in the portion of checkpoint processing where files are written to disk, in milliseconds | Enabled
PG Background Writer StatisticsPG Background Writer Statistics | Time that has been spent checkpoint where files are synchronized to disk | Total amount of time that has been spent in the portion of checkpoint processing where files are synchronized to disk, in milliseconds | Enabled
PG Background Writer Statistics | Number of times the background writer stopped a cleaning scan | Number of times the background writer stopped a cleaning scan because it had written too many buffers | Enabled
PG Background Writer Statistics | Number of times a backend had to execute its own fsync call | Number of times a backend had to execute its own fsync call (normally the background writer handles those even when the backend does its own write) | Enabled
PG Background Writer Statistics | Number of scheduled checkpoints that have been performed | Number of scheduled checkpoints that have been performed | Enabled
PG Background Writer Statistics | Number of requested checkpoints that have been performed | Number of requested checkpoints that have been performed | Enabled
PG Background Writer Statistics | Number of buffers written during checkpoints | Number of buffers written during checkpoints | Enabled
PG Background Writer Statistics | Number of buffers written directly by a backend | Number of buffers written directly by a backend | Enabled
PG Background Writer Statistics | Number of buffers written by the background writer | Number of buffers written by the background writer | Enabled
PG Background Writer Statistics | Number of buffers allocated | Number of buffers allocated | Enabled
PG Connections | Waiting connections |  | Enabled
PG Connections | Total connections |  | Enabled
PG Connections | Prepared connections | pg_prepared_xacts contains one row per prepared transaction. An entry is removed when the transaction is committed or rolled back.  When the pg_prepared_xacts view is accessed, the internal transaction manager data structures are momentarily locked, and a copy is made for the view to display. This ensures that the view produces a consistent set of results, while not blocking normal operations longer than necessary. Nonetheless there could be some impact on database performance if this view is frequently accessed. | Disabled
PG Connections | Idle in transaction connections | idle in transaction: The backend is in a transaction, but is not currently executing a query. | Enabled
PG Connections | Idle connections | idle: The backend is waiting for a new client command | Enabled
PG Connections | Active connections | active: The backend is executing a query | Enabled
PG General Informations | Uptime |  | Enabled
PG General Informations | Ping |  | Enabled
PG General Informations | Number of postgres processes |  | Enabled
PG General Informations | Cache hit ratio |  | Enabled
PG Other Informations | Number of pending updates from PGDG repository |  | Disabled
PG Streaming Replication | Streaming state |  | Enabled
PG Streaming Replication | Streaming replication count |  | Enabled
PG Transactions | Waiting transactions |  | Enabled
PG Transactions | 	Prepared Transactions |  | Disabled
PG Transactions | Oldest XID age |  | Enabled
PG Transactions | Oldest transaction running time |  | Enabled
PG Transactions | Idle transactions |  | Enabled
PG Transactions | 	Active transactions |  | Enabled
PG WAL Informations | WAL file count |  | Enabled
PG WAL Informations | Size of WAL files that ready to archive |  | Enabled
PG WAL Informations | Number of WAL file that ready to archive |  | Enabled
PG WAL Informations | Number of WAL files are failed to archive |  | Enabled
PG WAL Informations | Number of WAL files are archived |  | Enabled
PG WAL Informations | Created WAL files until now |  | Enabled
