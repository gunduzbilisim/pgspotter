# pgSpotter

pgSpotter is a `Zabbix template` that allows `monitoring PostgreSQL and pgBouncer`.

With this template, you can monitor your PostgreSQL and pgBouncer servers, including jobs created by pgAgent and pg_cron.

! Supported Zabbix versions: 4.4+

## Item, Trigger, Discovery etc. list

* Item list: `information/ITEMS.md`
* Trigger list: `information/TRIGGERS.md`
* Discovery rules: `information/DISCOVERIES.md`
* Item prototype list: `information/ITEM-PROTOTYPES.md`
* Trigger prototype list: `information/TRIGGER-PROTOTYPES.md`
* Macro list: `information/MACROS.md`

## Prerequisites

Before you begin, ensure you have met the following requirements:
* Please read all of the manual because if you want to set up differently, you can find some tips between the lines. :)
* You have installed the `zabbix-agent` package or built with source code.
* You are running `Linux`.
* You are running `PostgreSQL 10/11/12/13` and/or `pgBouncer`

## Notes:
- Some items work only in PostgreSQL 12+ because of new features.
- Some triggers are disabled by default for less resource consumption. If you want to use them, you must re-enable them.
- Some graphs are disabled. It is because when discovery executed, there will be tons of graphs. You can re-enable them, but we recommend that you create your own vector graphs.

## Installing pgSpotter for PostgreSQL

To install pgSpotter, follow these steps:

1. Clone this repository under `/etc/zabbix` or somewhere else.
```
cd /etc/zabbix
git clone https://gitlab.com/gunduzbilisim/pgspotter.git
```
* _*NOTE:*_ If you want to use another script path, you must change `{$SCRIPT_PATH}` variable from the template's macro section.

2. Change ownership of all files to `zabbix`.
```
chown -R zabbix: /etc/zabbix/pgspotter
```

3. Copy the `Zabbix Agent` configuration file to `/etc/zabbix/zabbix_agent.d` directory:
```
cp /etc/zabbix/pgspotter/zabbix_agentd.d/pgspotter.conf /etc/zabbix/zabbix_agentd.d/pgspotter.conf
```

4. Restart the Zabbix agent to recognize the UserParameters:
```
systemctl restart zabbix-agent.service
```

5. Create a PostgreSQL user to run some SQL commands for monitoring purposes:
```
CREATE USER pgspotter IN ROLE pg_monitor;
```
* _*NOTE:*_ If you want to use another user for monitoring, you must change `{$PG_USER}` variable from the template's macro section.
* _*NOTE:*_ If you want to work `Schema Discovery` and `Table Discovery` related settings, you must give `USAGE` permissions on all schemas to `pgspotter` user;

6. Add this line to `pg_ident.conf`:
```
monitoring zabbix pgspotter
```

7. Add this line to `pg_hba.conf`:
```
local all pgspotter ident map=monitoring
```

8. Import the `pgSpotter-PostgreSQL_Template.xml` file to your `Zabbix` from `Zabbix Web Interface`. These templates use the following `rules`:
* Templates
* Template screens
* Applications
* Items
* Discovery rules
* Triggers
* Graphs
* Screens


## Installing pgSpotter for pgBouncer

To install pgSpotter, follow these steps:

1. Clone this repository to `/etc/zabbix` or somewhere else.
```
cd /etc/zabbix
git clone https://gitlab.com/gunduzbilisim/pgspotter.git
```
* _*NOTE:*_ If you want to use another script path, you must change `{$SCRIPT_PATH}` variable from the template's macro section.

2. Change ownership of all files to `zabbix`.
```
chown -R zabbix: /etc/zabbix/pgspotter
```

3. Copy the `Zabbix Agent` configuration file to `/etc/zabbix/zabbix_agent.d` directory:
```
cp /etc/zabbix/pgspotter/zabbix_agentd.d/pgspotter.conf /etc/zabbix/zabbix_agentd.d/pgspotter.conf
```

4. Restart the Zabbix agent to recognize the UserParameters:
```
systemctl restart zabbix-agent.service
```

5. Create a PostgreSQL user to run some SQL commands for monitoring purposes:
```
CREATE USER pgspotter IN ROLE pg_monitor;
```
* _*NOTE:*_ If you want to use another user for monitoring, you must change `{$PB_USER}` variable from the template's macro section.

7. Add this line to `pg_hba.conf`:
```
local all pgspotter ident map=monitoring
```

6. Add this line to a file that you specify`hba_file` parameter in `pgbouncer.ini` :
```
local pgbouncer pgspotter trust
```



8. Import the `pgSpotter-PostgreSQL_Template.xml` file to your `Zabbix` from `Zabbix Web Interface`. These templates use the following `rules`:
* Templates
* Template screens
* Applications
* Items
* Discovery rules
* Triggers
* Graphs
* Screens

## Contributing to pgSpotter

To contribute to pgSpotter, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name><location>`
5. Create the pull request.

Alternatively see the GitLab documentation on [creating a merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).

## Contributors

Thanks to the following people who have contributed to this project:

* [@seda.halacli](https://gitlab.com/sedahalaclii)
* [@devrimgunduz](https://gitlab.com/devrimgunduz)
